import React from 'react';
import Carousel from 'react-bootstrap/Carousel';

const Movi = () => {

  return (
    <div>
      <Carousel data-bs-theme="dark">
      <Carousel.Item>
        <img
          className="d-block w-100"
          src="images/004.jpg"
          alt="First slide"
          style={{height: 500}}
        />
        <Carousel.Caption>
          <h5>Vintage Jeans Trouser</h5>
        </Carousel.Caption>
      </Carousel.Item>
      <Carousel.Item>
        <img
          className="d-block w-100"
          src="images/shirt1.jpg"
          alt="Second slide"
          style={{height: 500}}
        />
        <Carousel.Caption>
          <h5>Vintage Jeans Shirt</h5>
        </Carousel.Caption>
      </Carousel.Item>
      <Carousel.Item>
        <img
          className="d-block w-100"
          src="images/shirt9.jpg"
          alt="Third slide"
          style={{height: 500}}
        />
        <Carousel.Caption>
          <h5>Vintage Jeans Shirt</h5>
        </Carousel.Caption>
      </Carousel.Item>
    </Carousel>
    </div>
  );
}

export default Movi;
