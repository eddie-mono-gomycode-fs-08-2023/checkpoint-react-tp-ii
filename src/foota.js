import React from 'react';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';

const Foota = () => {
  return (
    <div>
      <Container fluid>
      <Row style={{height: 300, backgroundColor: 'black', marginTop: 100, display: 'flex', color: 'white', fontSize: 20, fontWeight: 'bold'}}>
        <Col >Contact uS</Col>
        <Col>About us</Col>
        <Col>Our Services</Col>
        <Col>Aims and Objective</Col>
      </Row>
    </Container>
    </div>
  );
}

export default Foota;
